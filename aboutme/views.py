from django.shortcuts import render
from .models import Person, Expertise, PhotoURL, Message
from .forms import Message_Form

# Create your views here.


response = {} 
def index(request):
    html = 'aboutme/aboutme.html'
    person = Person.objects.create(name="Kezia Irene", birthdate="1998-10-15", gender="Female", description="A person who always tries to be a better one", email="irenekezia98@.com")
    expertise = Expertise.objects.create(expertise ="Java and Python")
    expertise = Expertise.objects.create(expertise ="Making Fun Music")
    expertise = Expertise.objects.create(expertise ="Business IT Plan")
    model_pic = expertise = PhotoURL.objects.create(model_pic ="https://0.academia-photos.com/22671060/6165818/6987836/s200_kezia_irene.tesiman.jpg_oh_6c416e6db6c808e5087a71965d1a881a_oe_54ffe395___gda___1426160583_a83ba5be6321a5da3e15978004b85cb7")

    
    response['author']= "Kezia Irene"
    response['name'] = Person.objects.all()[0].name
    response['birthdate'] = Person.objects.all()[0].birthdate
    response['gender'] = Person.objects.all()[0].gender
    response['expertise'] = Expertise.objects.all()[0:3]
    response['description'] = Person.objects.all()[0].description
    response['email'] = Person.objects.all()[0].email
    response['model_pic'] = PhotoURL.objects.all()[0].model_pic

    response['message_form'] = Message_Form
    return render(request, html, response)

def message_post(request):
    form = Message_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Anonymous"
        response['email'] = request.POST['email'] if request.POST['email'] != "" else "Anonymous"
        response['message'] = request.POST['message']
        message = Message(name=response['name'], email=response['email'],
                          message=response['message'])
        message.save()
        html ='aboutme/form_result.html'
        return render(request, html, response)
    else:        
        return HttpResponseRedirect('/aboutme/')

def message_table(request):
    message = Message.objects.all()
    response['message'] = message
    html = 'aboutme/table.html'
    return render(request, html,response)
